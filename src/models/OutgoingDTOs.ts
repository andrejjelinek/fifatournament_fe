import { PlayerInDto, TypeOfMatchesInDto } from "./IncomingDTOs";

export interface NewMatchOutDto {
  firstPlayerName: string;
  secondPlayerName: string;
  tournamentId: string;
  matchType: TypeOfMatchesInDto;
  groupId: string;
}

export interface UpdateMatchOutDto {
  matchId: string;
  firstPlayer: PlayerInDto;
  secondPlayer: PlayerInDto;
  pointsPlayer1: number;
  pointsPlayer2: number;
  matchType: MatchType;
}

export interface UpdatePlayerOutDto {
  id: string;
  name: string;
}

export interface NewTournamentOutDto {
  name: string;
  date: string;
  description: string;
}

export enum MatchType {
  basic = "BASIC",
  eightFinal = "EIGHT_FINAL",
  quaterFinal = "QUATER_FINAL",
  semiFinal = "SEMI_FINAL",
  final = "FINAL",
}
