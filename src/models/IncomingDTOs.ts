import { MatchType } from "./OutgoingDTOs";

export interface PlayerInDto {
  id: string;
  name: string;
  goals: number;
}

export interface MatchInDto {
  id: string;
  groupId: string;
  player1: PlayerInDto;
  player2: PlayerInDto;
  hasWinner: boolean;
  isFromLoosed: boolean;
}

export interface MatchTypeInDto {
  id: string;
  matchType: TypeOfMatchesInDto;
  countOfMatches: number;
  matches: MatchInDto[];
}

export interface TypeOfMatchesInDto {
  id: number;
  type: MatchType;
  name: string;
  countOfMatches: number;
}

export interface TournamentInDto {
  id: string;
  name: string;
  date: string;
  description: string;
  countOfPlayers: number;
}

export interface PlayerScoreInDto {
  playerId: string;
  playerName: string;
  points: number;
}
