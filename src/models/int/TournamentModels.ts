export interface Tournament {
  id: string;
  name: string;
  date: string;
  description: string | null;
  Match: Match[];
}

export interface Match {
  id: string;
  matchDate: string;
  result: string | null;
  matchTypeId: string;
  winnerPlayerId: string | null;
  score: string | null;
  tournamentId: string;
}
