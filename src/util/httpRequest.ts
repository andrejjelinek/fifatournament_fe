import { QueryClient } from "@tanstack/react-query";
import axios from "axios";
import { ServiceResponse } from "../models/ServiceResponse";

export const queryClient = new QueryClient();

/**
 * Send request to backend api with axios
 * @param url url of controller with params
 * @param requestType type of request
 * @param data data for post and put request
 * @returns
 */
export const sendRequest = async <T>(
  url: string,
  requestType: "GET" | "POST" | "DELETE" | "PUT",
  data?: any
) => {
  try {
    let result;
    const controller = new AbortController();
    const api = axios.create({
      // baseURL: "http://localhost:3000/",
      baseURL: "https://fifa-tournament-be.onrender.com",
    });

    switch (requestType) {
      case "GET":
        result = await api.get<ServiceResponse<T>>(url);
        break;
      case "POST":
        result = await api.post<ServiceResponse<T>>(url, data);
        break;
      case "DELETE":
        result = await api.delete<ServiceResponse<T>>(url);
        break;
      case "PUT":
        result = await api.put<ServiceResponse<T>>(url, data);
        break;
      default:
        result = null;
        throw new Error("Invalid request type");
    }
    controller.abort();
    return result.data;
  } catch (error) {
    const errorResponse: ServiceResponse<T> = {
      data: null,
      message: `ERROR SEND REQUEST: ${error}`,
      success: false,
    };
    return errorResponse;
  }
};
