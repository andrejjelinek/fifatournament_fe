import { ThemeProvider, createTheme } from "@mui/material";
import Tournaments from "./components/tournament/Tournaments";
import "./main.scss";

function App() {
  const theme = createTheme({
    palette: {
      primary: {
        main: "#20c997",
      },
      secondary: {
        main: "#0ca678",
      },
      mode: "dark",
    },
  });

  return (
    <ThemeProvider theme={theme}>
      <main className="main-container">
        <Tournaments />
      </main>
    </ThemeProvider>
  );
}

export default App;
