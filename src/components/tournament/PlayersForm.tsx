import { Button, TextField } from "@mui/material";
import { useForm } from "react-hook-form";
import styles from "./styles/PlayersForm.module.scss";
import { MatchInDto, TypeOfMatchesInDto } from "../../models/IncomingDTOs";
import SaveIcon from "@mui/icons-material/Save";
import { useEffect } from "react";
import { NewMatchOutDto, UpdatePlayerOutDto } from "../../models/OutgoingDTOs";
import { toast } from "react-toastify";
import { queryClient, sendRequest } from "../../util/httpRequest";

type PlayerForm = {
  player1: string;
  player2: string;
};

interface IProps {
  match: MatchInDto | null;
  groupId: string;
  matchType: TypeOfMatchesInDto;
}

const PlayersForm: React.FC<IProps> = (props) => {
  const { match, groupId, matchType } = props;
  const { register, handleSubmit, setValue } = useForm<PlayerForm>();

  const handleSavePlayers = async (data: PlayerForm) => {
    if (!match) {
      toast.error("Zápas je NULL!");
      return;
    }
    const players: UpdatePlayerOutDto[] = [
      { id: match.player1.id, name: data.player1 },
      { id: match.player2.id, name: data.player2 },
    ];

    const result = await sendRequest<string>(
      "updatePlayersInfo",
      "POST",
      players
    );

    if (result.success) {
      queryClient.invalidateQueries({
        queryKey: ["matches"],
      });
      queryClient.invalidateQueries({
        queryKey: ["matchesWithLoose"],
      });
      toast.success("OK - mená hráčov boli upravené.");
    } else {
      toast.error(result.message);
    }
  };

  const handleCreateMatch = async (data: PlayerForm) => {
    const tournamentId = localStorage.getItem("tournamentId");
    if (!tournamentId) {
      toast.error("Nie je zvolený turnaj!");
      return;
    }

    const newMatch: NewMatchOutDto = {
      firstPlayerName: data.player1,
      secondPlayerName: data.player2,
      groupId: groupId,
      matchType: matchType,
      tournamentId: tournamentId,
    };

    const result = await sendRequest<string>("newMatch", "POST", newMatch);

    if (result.success) {
      queryClient.invalidateQueries({
        queryKey: ["matches"],
      });
      queryClient.invalidateQueries({
        queryKey: ["matchesWithLoose"],
      });
      toast.success("OK - zápas bol vytvorený.");
    } else {
      toast.error(result.message);
    }
  };

  useEffect(() => {
    if (match) {
      setValue("player1", match.player1.name);
      setValue("player2", match.player2.name);
    }
  }, [match]);

  return (
    <form
      className={styles["form"]}
      onSubmit={handleSubmit(
        match != null ? handleSavePlayers : handleCreateMatch
      )}
    >
      <div className={styles["form__inputs"]}>
        <TextField
          size="small"
          InputLabelProps={{
            style: { fontSize: 14 },
          }}
          InputProps={{
            style: { fontSize: 16 },
          }}
          variant="outlined"
          {...register("player1", { required: true })}
          label="Hráč 1"
        />
        <TextField
          size="small"
          InputLabelProps={{
            style: { fontSize: 14 },
          }}
          InputProps={{
            style: { fontSize: 16 },
          }}
          variant="outlined"
          {...register("player2", { required: true })}
          label="Hráč 2"
        />
      </div>
      <Button
        className={` ${styles["form__button"]}`}
        type="submit"
        autoFocus
        startIcon={<SaveIcon />}
        color="success"
        variant="contained"
      >
        {match != null ? "Upraviť" : "Vytvoriť"}
      </Button>
    </form>
  );
};

export default PlayersForm;
