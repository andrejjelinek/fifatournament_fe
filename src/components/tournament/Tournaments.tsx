import { useQuery } from "@tanstack/react-query";
import { toast } from "react-toastify";
import { MatchTypeInDto } from "../../models/IncomingDTOs";
import { MatchType } from "../../models/OutgoingDTOs";
import { sendRequest } from "../../util/httpRequest";
import Match from "./Match";
import NewTournament from "./NewTournament";
import TournamentSelect from "./TournamentSelect";
import styles from "./styles/Tournament.module.scss";
import TournamentStats from "../stats/TournamentStats";
import Loading from "../global/Loading";

const Tournaments = () => {
  const { data: matches } = useQuery({
    queryKey: ["matches"],
    queryFn: () => loadTournamentMatches(false),
  });

  const { data: matchesWithLoose } = useQuery({
    queryKey: ["matchesWithLoose"],
    queryFn: () => loadTournamentMatches(true),
  });

  const getMatches = (matchType: MatchTypeInDto, isFromLoosed: boolean) => {
    const matches = [];
    let className = "";
    if (isFromLoosed) {
      switch (matchType.matchType.type) {
        case MatchType.quaterFinal:
          className += "col-3";
          break;
        case MatchType.semiFinal:
          className += "col-2";
          break;
        case MatchType.final:
          className += "col-1";
          break;
        default:
          break;
      }
    } else {
      switch (matchType.matchType.type) {
        case MatchType.eightFinal:
          className += "col-4";
          break;
        case MatchType.quaterFinal:
          className += "col-5";
          break;
        case MatchType.semiFinal:
          className += "col-6";
          break;
        case MatchType.final:
          className += "col-7";
          break;
        default:
          break;
      }
    }

    let groupId = "1-a";
    let indexGroup = 1;
    for (let i = 0; i < matchType.countOfMatches; i++) {
      const index = i + 1;

      matches.push(
        <Match
          key={i}
          columnClassName={`${className}`}
          rowClassName={`${className}__match-${index}`}
          matchType={matchType.matchType}
          groupId={groupId}
          match={matchType.matches[i] ?? null}
        />
      );
      if (index % 2 == 0) {
        indexGroup += 1;
        groupId = `${indexGroup}-a`;
      } else {
        groupId = `${indexGroup}-b`;
      }
    }

    return matches;
  };

  const getMatchTypeClass = (
    matchType: MatchTypeInDto,
    isFromLoosed: boolean
  ): string => {
    let className = "tournament__";

    if (isFromLoosed) {
      switch (matchType.matchType.type) {
        case MatchType.quaterFinal:
          className += "col-3";
          break;
        case MatchType.semiFinal:
          className += "col-2";
          break;
        case MatchType.final:
          className += "col-1";
          break;
        default:
          break;
      }
    } else {
      switch (matchType.matchType.type) {
        case MatchType.eightFinal:
          className += "col-4";
          break;
        case MatchType.quaterFinal:
          className += "col-5";
          break;
        case MatchType.semiFinal:
          className += "col-6";
          break;
        case MatchType.final:
          className += "col-7";
          break;
        default:
          break;
      }
    }
    return className;
  };

  const loadTournamentMatches = async (isFromLoosed: boolean) => {
    const tournamentId = localStorage.getItem("tournamentId");
    console.log(tournamentId);
    if (!tournamentId) {
      return [];
    }
    const result = await sendRequest<MatchTypeInDto[]>(
      `matches/${tournamentId}/${isFromLoosed}`,
      "GET"
    );

    if (!result.success) {
      toast.error(result.message);
    }

    let data: MatchTypeInDto[] = result.data ?? [];
    if (isFromLoosed) {
      data =
        result?.data?.filter((item) => item.matchType.type != "EIGHT_FINAL") ??
        [];
    }
    return data;
  };

  return (
    <div className={styles["tournament"]}>
      <div className={styles["tournament__info"]}>
        <Loading />
        <div className={styles["tournament__info-select"]}>
          <TournamentStats />
          <NewTournament />
          <TournamentSelect />
        </div>
      </div>
      {matchesWithLoose?.map((matchType) => (
        <div
          key={matchType.id}
          className={`${styles["tournament__match-type"]} ${
            styles[getMatchTypeClass(matchType, true)]
          }`}
        >
          <div className={styles["tournament__matches"]}>
            {/* <p className={styles["tournament__matches--title"]}>
                {matchType.matchType.name}
              </p> */}
            {getMatches(matchType, true)}
          </div>
        </div>
      ))}

      {matches?.map((matchType) => (
        <div
          key={matchType.id}
          className={`${styles["tournament__match-type"]} ${
            styles[getMatchTypeClass(matchType, false)]
          }`}
        >
          <div className={styles["tournament__matches"]}>
            {/* <p className={styles["tournament__matches--title"]}>
                {matchType.matchType.name}
              </p> */}
            {getMatches(matchType, false)}
          </div>
        </div>
      ))}
    </div>
  );
};

export default Tournaments;
