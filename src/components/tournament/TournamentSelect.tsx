import { useQuery } from "@tanstack/react-query";
import { queryClient, sendRequest } from "../../util/httpRequest";
import { TournamentInDto } from "../../models/IncomingDTOs";
import { toast } from "react-toastify";
import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import { useEffect, useState } from "react";
import moment from "moment";
import styles from "./styles/Tournament.module.scss";

const TournamentSelect = () => {
  const [tournamentId, setTournamentId] = useState<string>("");
  const { data } = useQuery({
    queryKey: ["tournaments"],
    queryFn: () => loadTournaments(),
  });

  const loadTournaments = async () => {
    const result = await sendRequest<TournamentInDto[]>(
      `getTournaments`,
      "GET"
    );

    if (!result.success) {
      toast.error(result.message);
    }

    return result.data ?? [];
  };

  useEffect(() => {
    const id = localStorage.getItem("tournamentId");
    if (id) {
      setTournamentId(id);
    }
  }, []);

  const tournament = data?.find((item) => item.id === tournamentId) ?? null;
  return (
    <div className={styles["tournament-select"]}>
      <FormControl fullWidth className={styles["tournament-select__select"]}>
        <InputLabel id="tournament-select" sx={{ fontSize: "1.5rem" }}>
          Turnaj
        </InputLabel>
        <Select
          labelId="tournament-select"
          id="demo-simple-select"
          size="small"
          value={tournamentId}
          label="Turnaj"
          onChange={(e) => {
            setTournamentId(e.target.value);
            localStorage.setItem("tournamentId", e.target.value);
            queryClient.invalidateQueries({
              queryKey: ["matches"],
            });
            queryClient.invalidateQueries({
              queryKey: ["matchesWithLoose"],
            });
          }}
          sx={{ fontSize: "2rem" }}
        >
          {data?.map((item) => (
            <MenuItem value={item.id} sx={{ fontSize: "1.5rem" }}>
              {item.name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>

      <p className={styles["tournament-select--date"]}>
        {moment(tournament?.date, "ddd, DD MMM YYYY HH:mm:ss [GMT]").format(
          "DD.MM.YYYY HH:mm"
        )}
      </p>
    </div>
  );
};

export default TournamentSelect;
