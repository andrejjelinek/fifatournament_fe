import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import { Button, Drawer, TextField } from "@mui/material";
import moment from "moment-timezone";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";
import { NewTournamentOutDto } from "../../models/OutgoingDTOs";
import { queryClient, sendRequest } from "../../util/httpRequest";
import styles from "./styles/NewTournament.module.scss";

type FormValues = {
  date: string;
  name: string;
  description: string;
};

const NewTournament = () => {
  const [isOpen, setIsOpen] = useState(false);
  const { register, reset, handleSubmit } = useForm<FormValues>();

  const handleCreateTournament = async (data: FormValues) => {
    console.log(data.date);
    const timeZone = "Europe/Bratislava";

    const f = new Date(data.date);
    const test = moment(f).tz(timeZone);
    console.log(test);
    const ff = test.toISOString();
    console.log(ff);

    const tournament: NewTournamentOutDto = {
      name: data.name,
      date: test.toISOString(),
      description: data.description,
    };

    const result = await sendRequest<string>(
      `newTournament`,
      "POST",
      tournament
    );

    if (result.success) {
      reset();
      queryClient.invalidateQueries({ queryKey: ["tournaments"] });
      toast.success("OK - turnaj bol vytvorený");
    } else {
      toast.error(result.message);
    }
  };

  return (
    <>
      <Button
        className={styles["add-button"]}
        variant="contained"
        size="small"
        startIcon={<AddCircleOutlineIcon fontSize="large" />}
        onClick={() => setIsOpen(true)}
      >
        Nový turnaj
      </Button>
      <Drawer anchor="left" open={isOpen} onClose={() => setIsOpen(false)}>
        <form
          className={styles["new-tournament"]}
          onSubmit={handleSubmit(handleCreateTournament)}
        >
          <p className={styles["new-tournament--title"]}>
            Vytvorenie nového turnaja
          </p>

          <TextField
            variant="outlined"
            className={`${styles["new-tournament--input"]}`}
            label="Dátum"
            type="datetime-local"
            InputLabelProps={{
              style: { fontSize: 14 },
              shrink: true,
            }}
            InputProps={{
              style: { fontSize: 16 },
            }}
            {...register("date", { required: true })}
          />

          <TextField
            variant="outlined"
            className={`${styles["new-tournament--input"]}`}
            label="Názov"
            InputLabelProps={{
              style: { fontSize: 14 },
            }}
            InputProps={{
              style: { fontSize: 16 },
            }}
            {...register("name", { required: true })}
          />

          <TextField
            variant="outlined"
            className={`${styles["new-tournament--input"]}`}
            label="Popis"
            InputLabelProps={{
              style: { fontSize: 14 },
            }}
            InputProps={{
              style: { fontSize: 16 },
            }}
            multiline
            rows={3}
            {...register("description", { required: true })}
          />

          <Button
            color="secondary"
            className={styles["add-button"]}
            variant="contained"
            size="small"
            startIcon={<AddCircleOutlineIcon fontSize="large" />}
            type="submit"
          >
            Uložiť
          </Button>
        </form>
      </Drawer>
    </>
  );
};

export default NewTournament;
