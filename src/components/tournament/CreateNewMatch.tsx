import CloseIcon from "@mui/icons-material/Close";
import SaveIcon from "@mui/icons-material/Save";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
} from "@mui/material";
import { FC, useEffect } from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";
import { MatchInDto, TypeOfMatchesInDto } from "../../models/IncomingDTOs";
import { UpdateMatchOutDto } from "../../models/OutgoingDTOs";
import { queryClient, sendRequest } from "../../util/httpRequest";
import PlayersForm from "./PlayersForm";
import styles from "./styles/CreateNewMatch.module.scss";
import { useMutation } from "@tanstack/react-query";

type FormValues = {
  scorePlayer1: number;
  scorePlayer2: number;
  pointsPlayer1: number;
  pointsPlayer2: number;
};

interface IProps {
  handleClose: () => void;
  match: MatchInDto | null;
  groupId: string;
  matchType: TypeOfMatchesInDto;
}

const CreateNewMatch: FC<IProps> = (props) => {
  const { handleClose, match, groupId, matchType } = props;
  const { register, handleSubmit, setValue } = useForm<FormValues>();
  const isUpdating = match != null;

  const updateMatchMutation = useMutation({
    mutationFn: (data: FormValues) => updateMatch(data),
  });

  const updateMatch = async (data: FormValues) => {
    console.log(data);
    if (!match) {
      toast.error("Zápas na editovanie je NULL!");
      return;
    } else if (
      data.pointsPlayer1 < 0 ||
      data.pointsPlayer2 < 0 ||
      data.scorePlayer1 < 0 ||
      data.scorePlayer2 < 0
    ) {
      toast.error("Nemôžete zadať negatívne číslo!");
      return;
    } else if (data.scorePlayer1 === data.scorePlayer2) {
      toast.error("Nemôže byť remíza!");
      return;
    }

    const updatedMatch: UpdateMatchOutDto = {
      matchId: match.id,
      firstPlayer: {
        id: match.player1.id,
        name: match.player1.name,
        goals: +data.scorePlayer1,
      },
      secondPlayer: {
        id: match.player2.id,
        name: match.player2.name,
        goals: +data.scorePlayer2,
      },
      pointsPlayer1: +data.pointsPlayer1,
      pointsPlayer2: +data.pointsPlayer2,
      matchType: matchType.type,
    };

    const result = await sendRequest<MatchInDto>(
      `updateMatch/${match.isFromLoosed}`,
      "POST",
      updatedMatch
    );

    if (result.success) {
      toast.success("OK - zápas bol upravený");
    } else {
      toast.error(result.message);
    }

    queryClient.invalidateQueries({
      queryKey: ["matches"],
    });
    queryClient.invalidateQueries({
      queryKey: ["matchesWithLoose"],
    });
    handleClose();
  };

  useEffect(() => {
    if (match) {
      setValue("scorePlayer1", match.player1.goals);
      setValue("scorePlayer2", match.player2.goals);
    }
  }, [match]);

  return (
    <Dialog open fullWidth maxWidth="md">
      <DialogTitle>
        <p className={styles["title"]}>
          {isUpdating ? "Úprava zápasu" : "Vytvorenie zápasu"}
        </p>
      </DialogTitle>
      <DialogContent>
        <PlayersForm match={match} groupId={groupId} matchType={matchType} />

        {isUpdating ? (
          <form
            className={styles["main-form"]}
            id="create-new-match"
            onSubmit={handleSubmit((data) => updateMatchMutation.mutate(data))}
          >
            <div className={styles["form"]}>
              <div className={styles["form__player"]}>
                <p className={styles["form__player--name"]}>
                  {match.player1.name}
                </p>

                <TextField
                  size="small"
                  InputLabelProps={{
                    style: { fontSize: 14 },
                  }}
                  InputProps={{
                    style: { fontSize: 16 },
                  }}
                  variant="outlined"
                  {...register("scorePlayer1")}
                  label="Počet gólov"
                  type="number"
                  disabled={match.hasWinner || match?.player2.id == null}
                />
                <TextField
                  size="small"
                  InputLabelProps={{
                    style: { fontSize: 14 },
                  }}
                  InputProps={{
                    style: { fontSize: 16 },
                  }}
                  variant="outlined"
                  {...register("pointsPlayer1", { required: true })}
                  label="Body hráč 1"
                  type="number"
                  disabled={match.hasWinner || match?.player2.id == null}
                />
              </div>
              <div className={styles["form__player"]}>
                <p className={styles["form__player--name"]}>
                  {match.player2.name}
                </p>

                <TextField
                  size="small"
                  InputLabelProps={{
                    style: { fontSize: 14 },
                  }}
                  InputProps={{
                    style: { fontSize: 16 },
                  }}
                  variant="outlined"
                  {...register("scorePlayer2")}
                  label="Počet gólov"
                  type="number"
                  disabled={match.hasWinner || match?.player2.id == null}
                />
                <TextField
                  size="small"
                  InputLabelProps={{
                    style: { fontSize: 14 },
                  }}
                  InputProps={{
                    style: { fontSize: 16 },
                  }}
                  variant="outlined"
                  {...register("pointsPlayer2", { required: true })}
                  label="Body hráč 2"
                  type="number"
                  disabled={match.hasWinner || match?.player2.id == null}
                />
              </div>
            </div>

            <Button
              className={` ${styles["form__button"]}`}
              type="submit"
              autoFocus
              startIcon={<SaveIcon />}
              color="success"
              variant="contained"
              disabled={
                match.hasWinner ||
                match?.player2.id == null ||
                updateMatchMutation.status === "pending"
              }
            >
              Uložiť
            </Button>
          </form>
        ) : null}
      </DialogContent>
      <DialogActions>
        <Button
          className={styles["button"]}
          onClick={handleClose}
          startIcon={<CloseIcon />}
          color="error"
          variant="outlined"
        >
          Zrušiť
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default CreateNewMatch;
