import EditIcon from "@mui/icons-material/Edit";
import { Button, TextField } from "@mui/material";
import { useState } from "react";
import { MatchInDto, TypeOfMatchesInDto } from "../../models/IncomingDTOs";
import { MatchType } from "../../models/OutgoingDTOs";
import CreateNewMatch from "./CreateNewMatch";
import styles from "./styles/Match.module.scss";

interface IProps {
  columnClassName: string;
  rowClassName: string;
  matchType: TypeOfMatchesInDto;
  groupId: string;
  match: MatchInDto | null;
}

const Match: React.FC<IProps> = (props) => {
  const [openCreateDialog, setOpenCreateDialog] = useState<boolean>();
  const idEditable =
    props.match == null && props.matchType.type != MatchType.eightFinal;

  const getWinner = () => {
    if (props.match?.hasWinner) {
      if (props.match.player1.goals > props.match.player2.goals) {
        return props.match.player1.id;
      }
      return props.match.player2.id;
    }

    return null;
  };

  const winnerId = getWinner();
  return (
    <>
      <div
        className={`${styles.match} ${styles[props.columnClassName]} ${
          styles[props.rowClassName]
        }`}
      >
        <div
          className={`${styles["match__player"]} ${
            winnerId === props.match?.player1.id ? styles["match__winner"] : ""
          }`}
        >
          <TextField
            className="input-textfield"
            size="small"
            variant="outlined"
            value={props.match?.player1.name ?? ""}
            inputProps={{ readOnly: true }}
          />
          <p>{props.match?.player1.goals}</p>
        </div>
        <div
          className={`${styles["match__player"]} ${
            winnerId === props.match?.player2.id ? styles["match__winner"] : ""
          }`}
        >
          <TextField
            className="input-textfield"
            size="small"
            variant="outlined"
            value={props.match?.player2.name ?? ""}
            inputProps={{ readOnly: true }}
          />

          <p>{props.match?.player2.goals}</p>
        </div>

        <div className={styles["match__new-match-button"]}>
          <Button
            variant="outlined"
            color="warning"
            onClick={() => setOpenCreateDialog(true)}
            disabled={idEditable}
            startIcon={<EditIcon fontSize="large" color="warning" />}
          >
            {props.match != null ? "Upraviť" : "Pridať zápas"}
          </Button>
        </div>
      </div>
      {openCreateDialog && (
        <CreateNewMatch
          handleClose={() => setOpenCreateDialog(false)}
          match={props.match}
          groupId={props.groupId}
          matchType={props.matchType}
        />
      )}
    </>
  );
};

export default Match;
