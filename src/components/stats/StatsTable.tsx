import {
  Button,
  Drawer,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import { useQuery } from "@tanstack/react-query";
import { sendRequest } from "../../util/httpRequest";
import { PlayerScoreInDto } from "../../models/IncomingDTOs";
import { toast } from "react-toastify";
import styles from "./styles/StatsTable.module.scss";

interface IProps {
  handleClose: () => void;
}

const StatsTable: React.FC<IProps> = (props) => {
  const { handleClose } = props;

  const tournamentId = localStorage.getItem("tournamentId");

  const { data: winPlayers } = useQuery({
    queryKey: ["winPlayers", tournamentId],
    queryFn: () => getTournamentStats(false),
  });

  const { data: loosePlayers } = useQuery({
    queryKey: ["loosePlayers", tournamentId],
    queryFn: () => getTournamentStats(true),
  });

  const getTournamentStats = async (isLoosedType: boolean) => {
    if (!tournamentId) {
      return [];
    }

    const result = await sendRequest<PlayerScoreInDto[]>(
      `points/${tournamentId}/${isLoosedType}`,
      "GET"
    );
    console.log(result.data);
    if (!result.success) {
      toast.error(result.message);
    }

    return result.data ?? [];
  };

  return (
    <Drawer anchor="left" open onClose={handleClose}>
      <div className={styles["stats"]}>
        <p className={styles["stats--title"]}>Skóre pravá časť</p>

        <TableContainer className={styles["stats__table"]}>
          <Table>
            <TableHead>
              <TableRow className={`bg_primary`}>
                <TableCell></TableCell>
                <TableCell className={styles["stats__table--head-cell"]}>
                  Meno
                </TableCell>
                <TableCell className={styles["stats__table--head-cell"]}>
                  Body
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {winPlayers?.map((row, index) => (
                <TableRow key={row.playerId}>
                  <TableCell className={styles["stats__table--cell"]}>
                    {index + 1}.
                  </TableCell>
                  <TableCell className={styles["stats__table--cell"]}>
                    {row.playerName}
                  </TableCell>
                  <TableCell className={styles["stats__table--cell"]}>
                    {row.points}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>

        <p className={styles["stats--title"]}>Skóre ľavá časť</p>

        <TableContainer className={styles["stats__table"]}>
          <Table>
            <TableHead>
              <TableRow className={`bg_primary`}>
                <TableCell></TableCell>
                <TableCell className={styles["stats__table--head-cell"]}>
                  Meno
                </TableCell>
                <TableCell className={styles["stats__table--head-cell"]}>
                  Body
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {loosePlayers?.map((row, index) => (
                <TableRow key={row.playerId}>
                  <TableCell className={styles["stats__table--cell"]}>
                    {index + 1}.
                  </TableCell>
                  <TableCell className={styles["stats__table--cell"]}>
                    {row.playerName}
                  </TableCell>
                  <TableCell className={styles["stats__table--cell"]}>
                    {row.points}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>

        <Button
          className={styles["stats--button"]}
          variant="outlined"
          size="small"
          onClick={handleClose}
          color="error"
        >
          Zavrieť
        </Button>
      </div>
    </Drawer>
  );
};

export default StatsTable;
