import { Button } from "@mui/material";
import { useState } from "react";
import StatsTable from "./StatsTable";
import SportsScoreIcon from "@mui/icons-material/SportsScore";
import styles from "./styles/StatsTable.module.scss";

const TournamentStats = () => {
  const [openStats, setOpenStats] = useState(false);

  return (
    <>
      <Button
        className={styles["score-button"]}
        variant="outlined"
        onClick={() => setOpenStats(true)}
        startIcon={<SportsScoreIcon fontSize="large" />}
      >
        Skóre
      </Button>
      {openStats ? (
        <StatsTable handleClose={() => setOpenStats(false)} />
      ) : null}
    </>
  );
};

export default TournamentStats;
