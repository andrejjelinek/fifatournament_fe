import { LinearProgress } from "@mui/material";
import { useIsFetching } from "@tanstack/react-query";

const Loading = () => {
  const fetching = useIsFetching();

  let progress = null;
  if (fetching) {
    progress = <LinearProgress />;
  }

  return <>{progress}</>;
};

export default Loading;
